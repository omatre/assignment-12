﻿using PostGradDBLibrary;
using PostGradModelLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;

namespace PostGradConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Create Data
            var professors = new List<Professor>()
                    {
                        new Professor()
                        {
                            FirstName = "Stephen",
                            LastName = "Hawking",
                            EmailAddress = "stephen.hawking@cambridge.co.uk",
                            Subject = "Astrophysics"
                        },
                        new Professor()
                        {
                            FirstName = "Severin",
                            LastName = "Snurp",
                            EmailAddress = "severin.snurp@hogwarts.co.uk",
                            Subject = "Dark Magic"
                        }
                    };

            var students = new List<Student>()
            {
                        new Student()
                        {
                            FirstName = "Ola",
                            LastName = "Matre",
                            EmailAddress = "ola.matre@ciber.no",
                            Professor = professors[0]
                        },
                        new Student()
                        {
                            FirstName = "Harry",
                            LastName = "Potter",
                            EmailAddress = "harry_potter@hotmail.com",
                            Professor = professors[1]
                        },
                        new Student()
                        {
                            FirstName = "Hermoine",
                            LastName = "Granger",
                            EmailAddress = "hermoine_granger@gmail.com",
                            Professor = professors[1]
                        },
                        new Student()
                        {
                            FirstName = "Anders",
                            LastName = "Matre",
                            EmailAddress = "andersmatre@gmail.com"
                        }
            };
            #endregion

            #region Save To Database
            try
            {
                using (var context = new PostGradDbContext())
                {
                    context.Student.AddRange(students);
                    context.Professor.AddRange(professors);

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            #endregion

            #region Retrieve Active Data
            List<Student> retrievedStudents = null;
            List<Professor> retrievedProfessors = null;

            try
            {
                using (var context = new PostGradDbContext())
                {
                    retrievedStudents = (from student in context.Student
                                         where student.Active == true
                                         select student).ToList();

                    retrievedProfessors = (from professor in context.Professor
                                           where professor.Active == true
                                           select professor).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            #endregion

            #region Print Data
            try
            {
                foreach (var student in retrievedStudents)
                {
                    Console.WriteLine(
                        $"{ student.LastName }, { student.FirstName } ({ student.EmailAddress })"
                    );
                    if (student.Professor != null)
                        Console.WriteLine($"\tSupervisor: Prof.{ student.Professor.LastName }");
                    Console.WriteLine();
                }

                foreach (var prof in retrievedProfessors)
                {
                    Console.WriteLine(
                        $"{ prof.LastName }, { prof.FirstName } ({ prof.EmailAddress })\n" +
                        $"\tStudents:"
                    );
                    if (prof.Students != null)
                    {
                        foreach (var student in prof.Students)
                        {
                            Console.WriteLine(
                                $"\t\t{ student.LastName }, { student.FirstName }"
                            );
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            #endregion
        }
    }
}
