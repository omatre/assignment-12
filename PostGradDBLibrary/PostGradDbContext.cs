﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using PostGradModelLibrary;
using System;
using System.Collections.Generic;
using System.Text;

namespace PostGradDBLibrary
{
    public class PostGradDbContext : DbContext
    {
        public DbSet<Professor> Professor { get; set; }
        public DbSet<Student> Student { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"Data Source=PC7596\SQLEXPRESS;
                                   Initial Catalog=PostGradDB; 
                                   Integrated Security=True;");
        }
    }
}
