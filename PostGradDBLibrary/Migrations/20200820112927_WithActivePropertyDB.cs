﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGradDBLibrary.Migrations
{
    public partial class WithActivePropertyDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Student",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Professor",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Professor");
        }
    }
}
