﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGradDBLibrary.Migrations
{
    public partial class InitialDB2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Student_Professor_Student",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Student_Student",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Student",
                table: "Student");

            migrationBuilder.AlterColumn<int>(
                name: "ProfessorId",
                table: "Student",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Student_ProfessorId",
                table: "Student",
                column: "ProfessorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Student_Professor_ProfessorId",
                table: "Student",
                column: "ProfessorId",
                principalTable: "Professor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Student_Professor_ProfessorId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Student_ProfessorId",
                table: "Student");

            migrationBuilder.AlterColumn<int>(
                name: "ProfessorId",
                table: "Student",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Student",
                table: "Student",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Student_Student",
                table: "Student",
                column: "Student");

            migrationBuilder.AddForeignKey(
                name: "FK_Student_Professor_Student",
                table: "Student",
                column: "Student",
                principalTable: "Professor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
