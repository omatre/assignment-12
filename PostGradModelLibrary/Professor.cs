﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace PostGradModelLibrary
{
    public class Professor : Person
    {
        [MaxLength(50)]
        [AllowNull]
        public string Subject { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
