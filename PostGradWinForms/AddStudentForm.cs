﻿using PostGradDBLibrary;
using PostGradModelLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PostGradWinForms
{
    public partial class AddStudentForm : Form
    {
        private List<Professor> professors;

        public AddStudentForm()
        {
            InitializeComponent();
            try
            {
                using (var context = new PostGradDbContext())
                {
                    professors = context.Professor.ToList();
                    foreach (var professor in professors)
                    {
                        SupervisorTB.Items.Add("Prof." + professor.LastName);
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO
            }
        }

        private void CreateStudentClick(object sender, EventArgs e)
        {
            NewStudent = new PostGradModelLibrary.Student()
            {
                FirstName = FirstNameTB.Text,
                LastName = LastNameTB.Text,
                EmailAddress = EmailTB.Text,
                Professor = professors.Find(t =>
                {
                    return SupervisorTB.SelectedItem.ToString().Equals("Prof." + t.LastName);
                })
            };

            this.Close();
        }

        private void CancelClick(object sender, EventArgs e)
        {
            NewStudent = null;
            this.Close();
        }
    }
}
