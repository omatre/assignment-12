﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using PostGradDBLibrary;
using PostGradModelLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PostGradWinForms
{
    public partial class MainWindow : Form
    {
        PostGradDbContext context;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddStudentClick(object sender, EventArgs e)
        {
            var StudentForm = new AddStudentForm();

            StudentForm.Show();

            if (StudentForm.NewStudent != null)
            {
                AddStudentToDB(StudentForm.NewStudent);
                UpdateDataGrid();
            }
        }

        private void UpdateStudentsClick(object sender, EventArgs e)
        {
            UpdateDataGrid();
        }

        private void DeleteSelectedClick(object sender, EventArgs e)
        {
            DeleteSelectedRows();
            UpdateDataGrid();
        }

        private void UpdateDataGrid()
        {
            try
            {
                using (context = new PostGradDbContext())
                {
                    context.Student.Load();
                    context.Professor.Load();
                    StudentsGrid.DataSource = context.Student.Local.ToBindingList();
                }
            }
            catch (Exception ex)
            {
                ErrorConsole.Text += ex.Message + "\n";
            }
        }

        private void AddStudentToDB(Student newStudent)
        {
            try
            {
                using (context = new PostGradDbContext())
                {
                    context.Add(newStudent);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorConsole.Text += ex.Message + "\n";
            }
        }

        private void DeleteSelectedRows()
        {
            try
            {
                using (context = new PostGradDbContext())
                {
                    for (int i = 0; i < StudentsGrid.SelectedRows.Count; i++)
                    {
                        var studentRow = StudentsGrid.SelectedRows[i];
                        var studentObject = context.Student
                            .Find(int.Parse(studentRow.Cells["Id"].Value.ToString()));
                        context.Student.Remove(studentObject);
                        ErrorConsole.Text += studentObject.FirstName + " " + 
                            studentObject.LastName + " have been removed";
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorConsole.Text += ex.Message + "\n";
            }
        }
    }
}
