﻿namespace PostGradWinForms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StudentsGrid = new System.Windows.Forms.DataGridView();
            this.AddStudentBtn = new System.Windows.Forms.Button();
            this.UpdateStudentsBtn = new System.Windows.Forms.Button();
            this.DeleteSelectedBtn = new System.Windows.Forms.Button();
            this.ErrorConsole = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.StudentsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // StudentsGrid
            // 
            this.StudentsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StudentsGrid.Location = new System.Drawing.Point(12, 12);
            this.StudentsGrid.Name = "StudentsGrid";
            this.StudentsGrid.RowHeadersWidth = 51;
            this.StudentsGrid.Size = new System.Drawing.Size(789, 297);
            this.StudentsGrid.TabIndex = 0;
            this.StudentsGrid.Text = "dataGridView1";
            // 
            // AddStudentBtn
            // 
            this.AddStudentBtn.Location = new System.Drawing.Point(12, 399);
            this.AddStudentBtn.Name = "AddStudentBtn";
            this.AddStudentBtn.Size = new System.Drawing.Size(259, 48);
            this.AddStudentBtn.TabIndex = 1;
            this.AddStudentBtn.Text = "Add";
            this.AddStudentBtn.UseVisualStyleBackColor = true;
            this.AddStudentBtn.Click += new System.EventHandler(this.AddStudentClick);
            // 
            // UpdateStudentsBtn
            // 
            this.UpdateStudentsBtn.Location = new System.Drawing.Point(277, 399);
            this.UpdateStudentsBtn.Name = "UpdateStudentsBtn";
            this.UpdateStudentsBtn.Size = new System.Drawing.Size(259, 48);
            this.UpdateStudentsBtn.TabIndex = 1;
            this.UpdateStudentsBtn.Text = "Update";
            this.UpdateStudentsBtn.UseVisualStyleBackColor = true;
            this.UpdateStudentsBtn.Click += new System.EventHandler(this.UpdateStudentsClick);
            // 
            // DeleteSelectedBtn
            // 
            this.DeleteSelectedBtn.Location = new System.Drawing.Point(542, 399);
            this.DeleteSelectedBtn.Name = "DeleteSelectedBtn";
            this.DeleteSelectedBtn.Size = new System.Drawing.Size(259, 48);
            this.DeleteSelectedBtn.TabIndex = 1;
            this.DeleteSelectedBtn.Text = "Delete Selected";
            this.DeleteSelectedBtn.UseVisualStyleBackColor = true;
            this.DeleteSelectedBtn.Click += new System.EventHandler(this.DeleteSelectedClick);
            // 
            // ErrorConsole
            // 
            this.ErrorConsole.Enabled = false;
            this.ErrorConsole.Location = new System.Drawing.Point(12, 316);
            this.ErrorConsole.Name = "ErrorConsole";
            this.ErrorConsole.Size = new System.Drawing.Size(788, 77);
            this.ErrorConsole.TabIndex = 2;
            this.ErrorConsole.Text = "";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 450);
            this.Controls.Add(this.ErrorConsole);
            this.Controls.Add(this.DeleteSelectedBtn);
            this.Controls.Add(this.UpdateStudentsBtn);
            this.Controls.Add(this.AddStudentBtn);
            this.Controls.Add(this.StudentsGrid);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(830, 497);
            this.MinimumSize = new System.Drawing.Size(830, 497);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            ((System.ComponentModel.ISupportInitialize)(this.StudentsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView StudentsGrid;
        private System.Windows.Forms.Button AddStudentBtn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button DeleteSelectedBtn;
        private System.Windows.Forms.Button UpdateStudentsBtn;
        private System.Windows.Forms.RichTextBox ErrorConsole;
    }
}